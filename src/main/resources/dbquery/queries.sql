
CREATE DATABASE `kgf` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `Organization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrgName` varchar(255) NOT NULL,
  `Verified` tinyint(1) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `org_name_uk_1` (`OrgName`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `User` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrganizationId` int(10) unsigned NOT NULL,
  `UserName` varchar(255) NOT NULL,
  `Password` text NOT NULL,
  `ContactNumber` varchar(15) NOT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Verified` int(11) NOT NULL,
  `AppType` varchar(45) DEFAULT NULL,
  `Imei` varchar(45) DEFAULT NULL,
  `UserType` int(10) DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` int(10) unsigned DEFAULT NULL,
  `UpdatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UpdatedBy` int(10) unsigned DEFAULT NULL,
  `Role` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserName_UNIQUE` (`UserName`),
  UNIQUE KEY `user_name_uk_1` (`UserName`),
  KEY `User_ContactNumber_index` (`ContactNumber`),
  KEY `User_Email_index` (`Email`),
  KEY `User_OrganizationId` (`OrganizationId`),
  CONSTRAINT `user_fk_1` FOREIGN KEY (`OrganizationId`) REFERENCES `Organization` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
