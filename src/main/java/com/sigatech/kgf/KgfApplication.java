package com.sigatech.kgf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class KgfApplication {

	public static void main(String[] args) {
		SpringApplication.run(KgfApplication.class, args);
	}



}
