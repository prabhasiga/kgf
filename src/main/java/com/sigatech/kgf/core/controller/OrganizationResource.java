package com.sigatech.kgf.core.controller;

import com.sigatech.kgf.core.service.OrganizationService;
import com.sigatech.kgf.model.dto.OrganizationDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/organizations")
public class OrganizationResource {

    private final OrganizationService organizationService;

    @PostMapping
    @Transactional
    public ResponseEntity<OrganizationDto> createOrganization(@Valid @RequestBody OrganizationDto organizationDto)throws Exception{

        return ResponseEntity.ok(organizationService.createOrganization(organizationDto));
    }


}
