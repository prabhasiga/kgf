package com.sigatech.kgf.core.controller;

import com.sigatech.kgf.core.service.UserService;
import com.sigatech.kgf.model.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/users")
public class UserResource {

    private final UserService userService;

    @PostMapping
    @Transactional
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto){

        return ResponseEntity.ok(userService.createUser(userDto));
    }


}
