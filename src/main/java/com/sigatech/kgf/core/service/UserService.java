package com.sigatech.kgf.core.service;

import com.sigatech.kgf.helper.convertor.UserConvertor;
import com.sigatech.kgf.model.dto.UserDto;
import com.sigatech.kgf.persistance.entity.User;
import com.sigatech.kgf.persistance.repo.app.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepo userRepo;

    public UserDto createUser(UserDto userDto) {

        User user = userRepo.save(UserConvertor.getUser(userDto));

        return UserConvertor.getUserDto(user);

    }
}
