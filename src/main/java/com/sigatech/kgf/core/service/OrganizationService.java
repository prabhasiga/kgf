package com.sigatech.kgf.core.service;

import com.sigatech.kgf.helper.convertor.OrganizationConvertor;
import com.sigatech.kgf.model.dto.OrganizationDto;
import com.sigatech.kgf.persistance.entity.Organization;
import com.sigatech.kgf.persistance.repo.app.OrganizationRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@RequiredArgsConstructor
@Component
public class OrganizationService {
    private final OrganizationRepo organizationRepo;

    public OrganizationDto createOrganization(OrganizationDto organizationDto){

        Organization organization = OrganizationConvertor.getOrganization(organizationDto);
        organization.setCreatedAt(new Date());
        organization.setDeleted(0);
        organization = organizationRepo.save(organization);
        return OrganizationConvertor.getOrganizationDto(organization);

    }
}
