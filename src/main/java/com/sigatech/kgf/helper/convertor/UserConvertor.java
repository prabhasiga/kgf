package com.sigatech.kgf.helper.convertor;

import com.sigatech.kgf.model.dto.UserDto;
import com.sigatech.kgf.persistance.entity.User;

public class UserConvertor {

    public static User getUser(UserDto userDto){
        return User.builder()
                .id(userDto.getUserId())
                .organizationId(userDto.getOrganizationId())
                .userName(userDto.getUserName())
                .password(userDto.getPassword())
                .contactNumber(userDto.getContactNumber())
                .email(userDto.getEmail())
                .role(userDto.getRole())
                .appType(userDto.getAppType())
                .imei(userDto.getImei())
                .userType(userDto.getUserType())
                .verified(userDto.getVerified() == null ? 0 :1)
                .build();
    }

    public static UserDto getUserDto(User user){
        return UserDto.builder()
                .userId(user.getId())
                .organizationId(user.getOrganizationId())
                .userName(user.getUserName())
                .password(user.getPassword())
                .contactNumber(user.getContactNumber())
                .email(user.getEmail())
                .role(user.getRole())
                .appType(user.getAppType())
                .imei(user.getImei())
                .userType(user.getUserType())
                .build();
    }
}
