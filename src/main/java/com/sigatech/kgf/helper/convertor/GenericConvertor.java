package com.sigatech.kgf.helper.convertor;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class GenericConvertor<E,M> {

    public E modelToEntityConvertor(M model, E e){
        Gson gson = new Gson();
        String objectStr = gson.toJson(model);
        return gson.fromJson(objectStr, e.getClass().getGenericSuperclass());
    }

    public M entityToModelConvertor(E entity, M m){
        Gson gson = new Gson();
        String objectStr = gson.toJson(entity);
        return gson.fromJson(objectStr, m.getClass().getGenericSuperclass());
    }


    public List<E> modelToEntityListConvertor(List<M> models, E e){
        Gson gson = new Gson();

        List<E> eList = new ArrayList<>();

        for(M model : models){
           eList.add(modelToEntityConvertor(model, e));
        }

        return eList;
    }

    public List<M> entityToModelListConvertor(List<E> entities, M model){
        Gson gson = new Gson();

        List<M> mList = new ArrayList<>();

        for(E entity : entities){
            mList.add(entityToModelConvertor(entity, model));
        }

        return mList;
    }
}
