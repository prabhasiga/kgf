package com.sigatech.kgf.helper.convertor;

import com.sigatech.kgf.model.dto.OrganizationDto;
import com.sigatech.kgf.persistance.entity.Organization;

public class OrganizationConvertor {

    public static Organization getOrganization(OrganizationDto organizationDto){
        return Organization.builder()
                .id(organizationDto.getOrgId())
                .orgName(organizationDto.getOrgName())
                .verified(organizationDto.getVerified())
                .deleted(organizationDto.getDeleted())
                .createdAt(organizationDto.getCreatedAt())
                .build();
    }

    public static OrganizationDto getOrganizationDto(Organization organization){
        return OrganizationDto.builder()
                .orgId(organization.getId())
                .orgName(organization.getOrgName())
                .verified(organization.getVerified())
                .deleted(organization.getDeleted())
                .createdAt(organization.getCreatedAt())
                .build();
    }
}
