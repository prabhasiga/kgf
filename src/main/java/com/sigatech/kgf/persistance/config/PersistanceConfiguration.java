package com.sigatech.kgf.persistance.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.HashMap;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.sigatech.kgf.persistance",
        entityManagerFactoryRef = "coreEntityManager",
        transactionManagerRef = "coreTransactionManager"
)
@Slf4j
public class PersistanceConfiguration {


    @Value("${persistence.user:#{null}}")
    private String username;
    @Value("${persistence.password:#{null}}")
    private String password;
    @Value("${persistence.databaseName:#{null}}")
    private String database;
    @Value("${persistence.serverName:#{null}}")
    private String server;
    @Value("${persistence.portNumber:#{null}}")
    private Integer port;
    @Value("${persistence.maximumPoolSize:#{null}}")
    private Integer maxPoolSize;
    @Value("${persistence.connectionTimeout:#{null}}")
    private Long connectionTimeout;
    @Value("${persistence.jdbcUrl:#{null}}")
    private String jdbcUrl;


    @Primary
    @Bean
    public javax.sql.DataSource ds1DataSource() {
        HikariDataSource ds = new HikariDataSource();
        ds.addDataSourceProperty("databaseName", database);
        ds.addDataSourceProperty("serverName", server);
        ds.addDataSourceProperty("portNumber", port);
        ds.setJdbcUrl(jdbcUrl);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setConnectionTimeout(connectionTimeout);
        ds.setMaximumPoolSize(maxPoolSize);
        return ds;
    }

    @Bean
    @Primary
    @Qualifier("coreEntityManager")
    public LocalContainerEntityManagerFactoryBean coreEntityManager() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(ds1DataSource());
        em.setPackagesToScan(
                "com.sigatech.kgf.persistance");
        HibernateJpaVendorAdapter vendorAdapter
                = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        return em;
    }


    @Primary
    @Bean
    @Qualifier("coreTransactionManager")
    public PlatformTransactionManager coreTransactionManager() {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                coreEntityManager().getObject());
        return transactionManager;
    }

}
