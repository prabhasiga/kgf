package com.sigatech.kgf.persistance.repo.app;

import com.sigatech.kgf.persistance.entity.Organization;
import com.sigatech.kgf.persistance.repo.BaseRepository;

public interface OrganizationRepo extends BaseRepository<Organization, Integer> {
}
