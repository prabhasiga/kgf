package com.sigatech.kgf.persistance.repo.app;

import com.sigatech.kgf.persistance.entity.User;
import com.sigatech.kgf.persistance.repo.BaseRepository;

public interface UserRepo extends BaseRepository<User, Integer> {
}
