package com.sigatech.kgf.persistance.entity;

import com.google.gson.Gson;
import com.sigatech.kgf.model.dto.OrganizationDto;
import com.sigatech.kgf.model.dto.UserDto;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.weaver.ast.Or;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Organization")
@Where(clause = "deleted=0")
@Builder
public class Organization implements Serializable {

    private static final long serialVersionUID = -3688403909982714871L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String orgName;

    private int verified;

    private int deleted;

    @Column(name = "createdAt")
    @CreationTimestamp
    private Date createdAt;

}
