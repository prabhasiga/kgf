package com.sigatech.kgf.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;
import com.sigatech.kgf.model.dto.UserDto;
import com.sigatech.kgf.model.enums.UserRoleEnum;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "User")
@Where(clause = "deleted=0")
@Builder
public class User implements Serializable {


    private static final long serialVersionUID = -3688403909982714871L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private Integer organizationId;

    @NotNull
    private String userName;

    @NotNull
    @Pattern(regexp="^[a-zA-Z0-9]{3,20}",message="length must be 3 to 20")
    private String password;

    private String contactNumber;

    @Email
    private String email;

    private Integer verified;

    @Enumerated(EnumType.STRING)
    private UserRoleEnum role;

    private String appType;

    private String imei;

    private Integer userType;

    private int deleted;

    @Column(name = "createdAt")
    @CreationTimestamp
    private Date createdAt;

    private Integer createdBy;

    @Column (name = "updatedAt")
    @UpdateTimestamp
    private Date updatedAt;

    private Integer updatedBy;

}
