package com.sigatech.kgf.model.dto;

import com.sigatech.kgf.persistance.entity.Organization;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationDto implements Cloneable{

    private Integer orgId;

    @NotNull
    private String orgName;

    private int verified;

    private int deleted;

    private Date createdAt;

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
