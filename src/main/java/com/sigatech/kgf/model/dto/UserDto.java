package com.sigatech.kgf.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sigatech.kgf.model.enums.UserRoleEnum;
import com.sigatech.kgf.persistance.entity.User;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Cloneable{

    private Integer userId;

    @NotNull
    private Integer organizationId;

    @NotNull
    private String userName;

    @NotNull
    @Pattern(regexp="^[a-zA-Z0-9]{3,20}",message="length must be 3 to 20")
    private String password;

    private String contactNumber;

    @Email
    private String email;

    private Integer verified;

    @Enumerated(EnumType.STRING)
    private UserRoleEnum role;

    private String appType;

    private String imei;

    private Integer userType;

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

