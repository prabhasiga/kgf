package com.sigatech.kgf.model.enums;


public enum UserRoleEnum {

    ADMIN,
    ORG_HEAD,
    ORG_EMPLOYEE
}
